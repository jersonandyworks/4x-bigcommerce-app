# 4x BigCommerce App integration

This is a BigCommerce app that allow to integrate 4x / TrustGuard Seal

It's meant to add trust seals after checkout.

## Installation

To install this app you need these requirements:
- PHP ([Installation Guide](https://www.php.net/manual/en/install.php))
- Composer ([Installation Guide](https://getcomposer.org/doc/00-intro.md))
- Laravel ([Installation Guide](https://laravel.com/docs/5.8))
- Local SSL Cert (Recommend Valet or Homestead to ease set up)
  - Mac OS: Valet ([Installation Guide](https://laravel.com/docs/5.8/valet))
  - Windows / Linux: Homestead ([Installation Guide](https://laravel.com/docs/5.8/homestead))

To install PHP dependancies:

```bash
composer install
```
And JS dependancies:
```bash
npm install

## Usage
To compile JS assets:
```bash
npm run dev
```
After compiling the app should be reachable at the site you are hosting the app on locally. i.e https://4x-bigcommerce-app.dev/

When running the app outside of BigCommerce, setting the follow environment variable will cause the app to use the local API credential (also in the .env file):
```
APP_ENV=local
``` 
Likewise, setting it to production will use only the credentials received during the OAuth handshake when the app is install on the BigCommerce store:
```
APP_ENV=production
```
