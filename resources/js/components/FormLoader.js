import React, { Component } from 'react';
import ReactDOM from 'react-dom';
export default class FormLoader extends Component {
render() {
return (
<div className="container">
    <div className="row justify-content-center">
        <div className="col-md-8">
            <div className="card">
                <div className="card-header">4X BigCommerce App</div>
                <div className="card-body">
                    <form>
                        <div class="form-group">
                            <label for="apikey">4X API Key:</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                            <small id="4xhelp" class="form-text text-muted">You can input your 4x API Key here.</small>
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
);
}
}
if (document.getElementById('bc-4x-form')) {
ReactDOM.render(<FormLoader />, document.getElementById('bc-4x-form'));
}